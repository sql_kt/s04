
-- a. Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "_%d%_";

-- b. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id;

-- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT album_title, name FROM albums JOIN artists ON artists.id = albums.artist_id WHERE album_title LIKE "_%a%_" ;

-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4 ;

-- f. Join the 'albums' and 'songs' tables.(Sort albums from Z-A.)
SELECT album_title, song_name FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title, song_name DESC;
